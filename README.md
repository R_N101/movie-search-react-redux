# Rajan Nahar

Please ensure you run 'npm install' from the root directory in order to install package dependencies.

## Overview
At the time of creating this application, the suggested API (OMDB API) was not generating an API key, which was required. As a result, I used an alternative API - [The Movie DB](https://api.themoviedb.org)

I used create-react-app to bootstrap this project to use the React library and used Redux for state management - to fetch the data from the API and to read text entered by the user.

I used the React-Materialize library to provide styled components - this combines React components with Material Design.


## About the project
* Using create-react-app allowed me to use React out-of-the-box, without the need to configure much - this is very useful when creating high-fidelty prototypes.
* I created some dummy components to ensure the application was rendering.
* I analysed the API and read documentation on how to use it
* I set up my application structure - folders/file names - actions, reducers, constants, etc.
* I using Node Package Manager to install Redux
* I set up my actions file which allows me to configure an API to fetch data - URL and API key configurable
* I set up my action dispatchers - these return functional objects - first I set up the action dispatcher which allowed me to fetch the data from the API
* I then created my reducer with the constants types and switch statements to assess these types, the state was spread and updated with the payload depending on the action type returned from the action dispatch
* I created my main reducer which using combineReducers to return one reducer to configure my store with (using createStore)
* I used Provider to provide the Redux store to the React application
* I connected my main React component (moviesComponent) to the store with mapStateToProps and mapDispatchToProps, as well as using connect.
* The data is fetched from the API using componentWillMount - this.props.getData() is fired
* I return an empty list and a input text box
* When text is entered into the text box, I use .filter() to match (includes and case insensitive) and return any matches, else null is returned
* The input text fires an event to dispatch an action and assigns any value, as entered, to the Redux Store
* I used React Router (router.js component which was provided to the root index.js component)
* I configured the root page (input search) and the child page (details on individual movies) with routes
* The child pages were configured with a path and a parameter (movie ID)
* The MoviesComponent component returns matchin resuls with a clickable buttons which take the user to /movies/{movie:id} and also passes movie data as Link state
* The MovieDetail (child) component takes the props and sets component state
* The data is passed into the component as it is rendered - I have also destructured objects to improve code clarity
* The MovieDetails component also has a button which takes the user back to MoviesComponent

## Improvements
* Analyse performance and investigate any room for improvement - for example, using React PureComponents
* Naturally the UI and UX could be improved
* Integrate unit testing into the application - using the likes of Jest
* Use more of the Redux store, less of the component state
* Improve on React routing to remember user history (backwards and forwards buttons in the browser)
* Use browser cache/localstorage to reduce the need to make API calls