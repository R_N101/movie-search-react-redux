import React from 'react';
import {BrowserRouter, Switch, Route} from 'react-router-dom';

import MoviesComponent from './moviesComponent';
import MovieDetail from './movieDetail';

const Router = () => (
    <BrowserRouter>
        <Switch>
            <Route path="/" component={MoviesComponent} exact />
            <Route path="/movie/:id" component={MovieDetail} />
        </Switch>
    </BrowserRouter>
);

export default Router;