import React from 'react';
import {Input} from 'react-materialize';
import PropTypes from 'prop-types';

const FilterTitle = props => (

    <Input className="filterTame" placeholder="Filter by title" s={12} onKeyUp={props.handleFilterName} />                

);

FilterTitle.propTypes = {
    handleFilterName: PropTypes.func.isRequired
}

export default FilterTitle;
