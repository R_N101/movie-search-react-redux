import React, {Component} from 'react';
import {Row, Col, Card, CardTitle} from 'react-materialize';
import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';

class MovieDetail extends Component {

    state = {
        movieId: "",
        title: "",
        releaseDate: "",
        poster: "",
        overview: ""
    }

    componentDidMount = () => {

        const {movieId, movieTitle, movieReleaseDate, moviePoster, movieOverview} = this.props.location.state;

        this.setState({
            movieId: movieId,
            title: movieTitle,
            releaseDate: movieReleaseDate,
            poster: `http://image.tmdb.org/t/p/w500/${moviePoster}`,
            overview: movieOverview
        })
    }

    render() {

        const {movieId, title, releaseDate, poster, overview} = this.state;

        return(
            <Row key={movieId}>
                <Col m={7} s={12}>
                    <Card 
                        horizontal 
                        header={<CardTitle image={poster}></CardTitle>}>
                        <h5>{title}</h5>
                        <small>{releaseDate}</small>
                        <p>{overview}</p>
                        <br/>
                        <Link to="/">Go Back</Link>
                    </Card>
                </Col>
            </Row>
        );

    }
}

MovieDetail.propTypes = {
    movieId: PropTypes.number.isRequired,
    movieTitle: PropTypes.string.isRequired,
    movieReleaseDate: PropTypes.string.isRequired,
    moviePoster: PropTypes.string.isRequired,
    movieOverview: PropTypes.string.isRequired
}

export default MovieDetail;