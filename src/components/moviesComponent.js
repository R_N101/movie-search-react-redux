import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Row, Preloader, Col, Card, Button} from 'react-materialize';
import {getData, filterTitle} from '../actions/index';
import FilterTitle from './filterTitle';
import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';

class MoviesComponent extends Component {

    state = {
        filterTitle: ""
    }

    componentWillMount = () => {
        this.props.getData()
    }

    handleFilterName = (event) => {
        let text = event.target.value;
        this.props.filterTitle(text)
        this.setState({
            filterTitle: text
        })
    };

    render() {

        const {movies} = this.props.movies;
        let matchingTitle = [];

        if (movies){

            this.state.filterTitle.length > 0
            ? matchingTitle = movies.filter(movie => 
                movie.title.toLowerCase().includes(
                    this.state.filterTitle.toLowerCase())
            )
            : matchingTitle = []
    
            return(

                <div className="App">
                    <Row>
                        <h1>Movie Search app</h1>
                    </Row>

                    <Row>
                        <FilterTitle handleFilterName={this.handleFilterName} />
                    </Row>

                    <Row>
                        <Col m={6} s={12}>
                        { matchingTitle.map(movie =>    
                            <Card 
                                className='blue-grey darken-1'
                                key={movie.id}
                                textClassName='white-text'
                                title={movie.title} 
                            >
                            <Button waves='light'>
                                <Link 
                                    to={{ 
                                        pathname: `/movie/${movie.id}`,
                                        state: { 
                                            movieId: movie.id,
                                            movieTitle: movie.title,
                                            movieReleaseDate: movie.release_date,
                                            moviePoster: movie.poster_path,
                                            movieOverview: movie.overview
                                        } 
                                    }}
                                    style={{color: "#fff"}}
                                >
                                    Click
                                </Link>
                            </Button>
                            </Card>
                        )}
                        </Col>
                    </Row>

                </div>
                    
            );
        } else {
            return(
                <Row>
                    <Preloader />
                </Row>
            );
        }
        

    }

}

MoviesComponent.propTypes = {
    movies: PropTypes.object.isRequired,
    filterTitle: PropTypes.func.isRequired
}

const mapStateToProps = state => ({
    movies: state.movies,
    filterTitle: state.filterTitle
})


const mapDispatchToProps = (dispatch) => {
	return {
		getData: () => {
			dispatch(getData());
		},
		filterTitle: (text) => {
			dispatch(filterTitle(text));
		},
	}
}

export default connect( mapStateToProps, mapDispatchToProps )(MoviesComponent);