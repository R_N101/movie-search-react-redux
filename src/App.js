import React, { Component } from 'react';
import {Row} from 'react-materialize';
import MoviesComponent from './components/moviesComponent';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Row>
          <h1>Movie Search app</h1>
        </Row>


        <MoviesComponent />

      </div>
    );
  }
}

export default App;
