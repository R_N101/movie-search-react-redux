import {GET_DATA, FILTER_TITLE} from '../constants/constants';

const api_key = "0bc642bafdd88ce1b64363a86b7b93a3"
const url = `https://api.themoviedb.org/3/movie/popular?api_key=${api_key}&language=en-US`
 
export const getData = () => dispatch => {
    return fetch(url)
    .then(response => response.json())
    .then(data => dispatch({
      type: GET_DATA, 
      payload: data
    }))
}


export const filterTitle = (text) => dispatch => {
  let title = text
  return dispatch({
    type: FILTER_TITLE,
    payload: title
  })
}