import { combineReducers } from 'redux';
import getDataReducer from './getDataReducer';
import filterTitleReducer from './filterTitleReducer';

const mainReducer = combineReducers({
    movies: getDataReducer,
    filterTitle: filterTitleReducer
});

export default mainReducer;