import { GET_DATA } from "../constants/constants";

const initialState = {}

export function getDataReducer(state=initialState, action) {

    const { type, payload } = action;

    switch(type) {
        case GET_DATA:
        return {
            ...state,
            movies: payload.results,
        };

        default:
        return state;
    }
}

export default getDataReducer;