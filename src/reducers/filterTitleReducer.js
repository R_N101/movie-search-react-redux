import { FILTER_TITLE } from "../constants/constants";

const initialState = {}

export function filterTitleReducer(state=initialState, action) {

    const { type, payload } = action;

    switch(type) {
        case FILTER_TITLE:
        // console.log("FILTER_TILE_REDUCER ", action)
        return {
            ...state,
            filterTitle: payload,
        };

        default:
        // console.log("default")
        return state;
    }
}

export default filterTitleReducer;